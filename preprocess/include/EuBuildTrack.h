/* 
 * File:   EuBuildTrack.h
 * Author: Daniel Kalin
 *
 * Created on 24. Januar 2014
 * 
 * v1.0
 */

#ifndef EUBUILDTRACK_H
#define	EUBUILDTRACK_H

#include "TBPreprocess.h"

#define USE_TRANSLATIONS true // Add a translation to track
#define USE_CODEDSHIFTS false // Add a hard coded shift to corect track pos (remove later)
#define CHI2MIN 999 // raw chi2 cut
#define NMATCHES 1 // Number of DUT whiche must be matched to a track
#define TRACKS -1 // Tracks count (-1 look at all tracks in event)

class EuBuildTrack : public TBPreprocess {
	
/** @class Reads in data from tbtrack class (at the moment only zspix and eutracks branches) */
private:
  std::list<int> matchIdens;
  
  double chi2cut;
  std::map<IDEN, int> lv1Min;
  std::map<IDEN, int> lv1Max;
  std::map<IDEN, double> zPos;
  
  //Track tree
  TTree* tracktree;

  int t_nTrackParams;
  int t_euEv;
  std::vector<double>* t_posX;
  std::vector<double>* t_posY;
  std::vector<double>* t_dxdz;
  std::vector<double>* t_dydz;
  std::vector<int>* t_iden;
  std::vector<int>* t_trackNum;
  std::vector<double>* t_chi2;
  std::vector<double>* t_ndof;

  //Pixel tree
  TTree* pixeltree;

  int p_nHits;
  int p_euEv;
  std::vector<int>* p_col;
  std::vector<int>* p_row;
  std::vector<int>* p_tot;
  std::vector<int>* p_iden;
  std::vector<int>* p_lv1;
  
  //euhits tree
  TTree* euhits;

  int e_nHits;
  std::vector<double>* e_zpos;
  std::vector<int>* e_sensorID;


public:
  EuBuildTrack()
  {
    name = "EuBuildTrack";
  };
  
  virtual void init(TBCore* core);
  virtual void initRun(TBCore* core);
  virtual void initEvent(TBCore* core);
  virtual void buildEvent(TBCore* core, TBEvent* event);
  virtual void finalizeRun(TBCore* core);
  virtual void finalize(TBCore* core);
  
private:
	void useTranslation(TBCore* core, int iden);
	void useHardCodedShift(TBCore* core, int iden);
};

// class factories
extern "C" TBPreprocess* create()
{
	return new EuBuildTrack;
}

extern "C" void destroy(TBPreprocess* tbp)
{
	delete tbp;
}


#endif	/* EUBUILDTRACK_H */

