/* 
 * File:   CheckAlign.h
 * Author: daniel
 *
 * Created on 17. Februar 2014, 19:42
 */

#ifndef CHECKALIGN_H
#define	CHECKALIGN_H

#include "TBPreprocess.h"

class CheckAlign: public TBPreprocess
{
private:
	bool doCuts;
	std::map<IDEN, TProfile*> h_hitxVresx;
	std::map<IDEN, TProfile*> h_hityVresx;
	std::map<IDEN, TProfile*> h_hitxVresy;
	std::map<IDEN, TProfile*> h_hityVresy;
	std::map<IDEN, TH1D*> h_resX;
	std::map<IDEN, TH1D*> h_resY;
	std::vector<int> badRuns;
	std::vector<int> run;
	std::map<IDEN, std::vector<double> > deltaX;
	std::map<IDEN, std::vector<double> > deltaY;
	
	std::map<IDEN, double> matchX;
	std::map<IDEN, double> matchY;
	std::map<IDEN, double> scaleMatchX;
	std::map<IDEN, double> scaleMatchY;
	std::map<IDEN, double> matchXscaled;
	std::map<IDEN, double> matchYscaled;

public:
	CheckAlign()
	{
		TBPreprocess::name = "CheckAlign";
	}
	virtual void init(TBCore* core);
	virtual void initRun(TBCore* core);
	virtual void buildEvent(TBCore* core, TBEvent* event);
	virtual void finalizeRun(TBCore* core);
	virtual void finalize(TBCore* core);
};

// class factories
extern "C" TBPreprocess* create()
{
	return new CheckAlign;
}

extern "C" void destroy(TBPreprocess* tbp)
{
	delete tbp;
}

#endif	/* CHECKALIGN_H */

