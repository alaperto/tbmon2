/* 
 * File:   Lv1Cut.cc
 * Author: daniel
 * 
 * Created on 15. Februar 2014, 14:41
 */

#include "Lv1Cut.h"

void Lv1Cut::init(const TBCore* core)
{
	int iden;
	int maxToT;

	for(auto dut: core->usedDUT)
	{
		iden = dut->iden;
		maxToT = dut->getMaxTot();

		Lv1Cut::h_lvl1HistAny[iden] = new TH1I("", ";All Hits Lvl1", maxToT+1, -.5, maxToT+.5);
		Lv1Cut::h_lvl1HistMatched[iden] = new TH1I("", ";Matched Hits Lvl1", maxToT+1, -.5, maxToT+.5);
	}
}

void Lv1Cut::event(const TBCore* core, const TBEvent* event)
{
	
	DUT* dut = event->dut;
	int iden = event->iden;
	
	for(auto hit: event->hits)
	{
		h_lvl1HistAny[iden]->Fill(hit->lv1);
	}
	
	for(auto tbtrack: event->tracks)
	{
		if(tbtrack->fMatchedCluster == kBad || tbtrack->fMatchedHit == kBad)
		{
			continue;
		}
		
		Lv1Cut::h_lvl1HistMatched[iden]->Fill(tbtrack->matchedHit->lv1);
	}
}

void Lv1Cut::finalize(const TBCore* core)
{
	core->output->processName = Lv1Cut::name;
	
	int iden;
	char* histoTitle = new char[500];
	for(auto dut: core->usedDUT)
	{
		iden = dut->iden;

		
		// set up cuts
		core->output->cuts = "lv1 of all raw hits";
		core->output->currentPreprocessCuts = "";
		// -----------
		std::sprintf(histoTitle, "Lv1 Dist Any DUT %i", iden);
		Lv1Cut::h_lvl1HistAny[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "lv1_dist_any_dut_%i", iden);
		Lv1Cut::h_lvl1HistAny[iden]->SetName(histoTitle);
		core->output->drawAndSave(Lv1Cut::h_lvl1HistAny[iden], "", "e");
		
		// set up cuts
		core->output->cuts = "lv1 matched hits";
		core->output->currentPreprocessCuts = "lv1 between "+std::to_string(dut->getlv1Min())+" and "+std::to_string(dut->getlv1Max());
		// -----------
		std::sprintf(histoTitle, "Lv1 Dist Matched DUT %i", iden);
		Lv1Cut::h_lvl1HistMatched[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "lv1_dist_matched_dut_%i", iden);
		Lv1Cut::h_lvl1HistMatched[iden]->SetName(histoTitle);
		core->output->drawAndSave(Lv1Cut::h_lvl1HistMatched[iden], "", "e");
		
		delete Lv1Cut::h_lvl1HistAny[iden];
		delete Lv1Cut::h_lvl1HistMatched[iden];
	}

	Lv1Cut::h_lvl1HistAny.clear();
	Lv1Cut::h_lvl1HistMatched.clear();
	delete[] histoTitle;
}

