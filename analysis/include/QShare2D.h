/* 
 * File:   QShare2D.h
 * Author: daniel
 *
 * Created on 7. März 2014, 14:28
 */

#ifndef QSHARE2D_H
#define	QSHARE2D_H

#include "TBAnalysis.h"

/**
 * @class produces 3 2d histograms to represent charge sharing
 * All histograms are plotted with respect to track coordinates, modulo 2 by 2 pixel size
 * 
 * @var h_anyHitTrkMap entry for every time the (from track coordinates) corresponding OR any adjacent pixel fires
 * @var h_sharedHitTrkMap entry for every time the corresponding pixels fires AND any other adjacent pixel fires (i.e. when charge is shared)
 * The third histogram is the ratio of entries in h_sharedHitTrkMap to h_anyHitTrkMap
 *
 * The analysis regards only the events with:
 * exactly one cluster
 * between 1 and 4 hits (inclusive) in this one cluster
 * a hit in the central region away from masked pixels
 * 
 */

class QShare2D: public TBAnalysis
{
private:
	std::map<IDEN, std::vector<TH2D*> > h_anyHitTrkMap;
	std::map<IDEN, std::vector<TH2D*> > h_sharedHitTrkMap;
	std::map<IDEN, std::vector<TH2D*> > h_anyHitTrkMap_mirror; //mirrored version to be aligned with eff and qeff plots
	std::map<IDEN, std::vector<TH2D*> > h_sharedHitTrkMap_mirror; //mirrored version to be aligned with eff and qeff plots
	std::map<IDEN, std::vector<TH3D*> > h_directCharge;
	std::map<IDEN, std::vector<TH3D*> > h_chargeShareScatter;
	std::map<IDEN, std::vector<int> > numTracksPixel;
	std::map<IDEN, int> numTracks;

	bool doCharge;

public:
	QShare2D()
	{
		TBAnalysis::name = "QShare2D";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);
};

// class factories
extern "C" TBAnalysis* create()
{
	return new QShare2D;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* QSHARE2D_H */

