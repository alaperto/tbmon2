/* 
 * File:   qEfficiency.h
 * Author: daniel
 *
 * Created on 8. März 2014, 15:28
 */

#ifndef QEFFICIENCY_H
#define	QEFFICIENCY_H

#include "TBAnalysis.h"

class qEfficiency: public TBAnalysis
{
private:
	std::map<int, std::vector<TH3D*> > h_totScatter;
	std::map<int, std::vector<TH3D*> > h_chargeScatter;
	std::map<int, std::vector<TH1D*> > h_chargeLandau;

	/*std::map<int, TH3D*> h_totScatterBiasLow;
	std::map<int, TH3D*> h_totScatterBiasHi;
	std::map<int, TH3D*> h_chargeScatterBiasLow;
	std::map<int, TH3D*> h_chargeScatterBiasHi;

	std::map<int, TH3D*> h_totScatterReadout;
	std::map<int, TH3D*> h_chargeScatterReadout;

	std::map<int, TH2D*> h_xyColumns;
	std::map<int, TH2D*> h_xyBulk;
	 */
	//Histogram resolution
	double xRes, yRes, totRes, qRes;
	//Other config
	std::map<int, double> electrodeWidth;
	std::map<int, double> xy_totCutoff;

	void zxProject(const TBCore* core, const char* histoTitle, const char* histoName, TH3D* h1, TH3D* h2 = NULL);
	//Common functions done oh-so-many times
	/*void fixTitles(TH3D* h, const char* Ztitle)
	{
		h->SetXTitle("Xmod position [#mu m]");
		h->SetYTitle("Ymod position [#mu m]");
		h->SetZTitle(Ztitle);
	}
	void zxProject(TH3D* h, const char* saveName, const TBCore& core, TH3D* h2 = NULL);

	void drawAspect(TH2D* h, const char* saveName, const TBCore& core, const Event &event);

	TProfile* makeLandauProfile(const TBCore& core, TH3D* h_scatter, int rebin = 1);
	*/
	
public:
	qEfficiency()
	{
		TBAnalysis::name = "qEfficiency";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);

};

Double_t langaufunction(Double_t *x, Double_t *par);

// class factories
extern "C" TBAnalysis* create()
{
	return new qEfficiency;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* QEFFICIENCY_H */

