/* 
 * File:   BeamProfile.h
 * Author: daniel
 *
 * Created on 9. Februar 2014, 05:13
 */

#ifndef BEAMPROFILE_H
#define	BEAMPROFILE_H

#include "TBAnalysis.h"

class BeamProfile: public TBAnalysis
{
private:
	std::map<IDEN, TH2D*> h_beamProfile;
	std::map<IDEN, TH2D*> h_beamProfileZoom;
	std::map<IDEN, TH2D*> h_beamProfileMasked;
	std::map<IDEN, TH1D*> h_chi2;
	std::map<IDEN, TH1D*> h_chi2Zoom;
	std::map<IDEN, TH1D*> h_chi2ZoomMatchedTracks;
	std::map<IDEN, TH2D*> h_dutHit;
	std::map<IDEN, TH2D*> h_angle;
	std::map<IDEN, TH2D*> h_angle_normalcuts;
	std::map<IDEN, TH2D*> h_beamProfile_noAngleCut;
	std::map<IDEN, TH2D*> h_TrackShift_XY;

	std::map<IDEN, TH2D*> h_corr_PosVsAngle_XX;
	std::map<IDEN, TH2D*> h_corr_PosVsAngle_XY;
	std::map<IDEN, TH2D*> h_corr_PosVsAngle_YX;
	std::map<IDEN, TH2D*> h_corr_PosVsAngle_YY;

public:
	BeamProfile()
	{
		TBAnalysis::name = "BeamProfile";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);
};

// class factories
extern "C" TBAnalysis* create()
{
	return new BeamProfile;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* BEAMPROFILE_H */

