/* 
 * File:   Residuals.h
 * Author: daniel
 *
 * Created on 7. März 2014, 18:12
 */

#ifndef RESIDUALS_H
#define	RESIDUALS_H

#include "TBAnalysis.h"
#include <unordered_map>

/**
 * This class creates so called residual plots which are useful to check the quality of the reconstruction and analysis as well as the telescope resolution. Basically a residual
 * is the difference between the calculated cluster center and the traversing point of its matched track.
 */
class Residuals: public TBAnalysis
{
private:
	enum
	{
		ETA, QMEAN, GEOMEAN, MAXTOT, DHT
	};
	static const int algNum = 5;
	static const TString algName[algNum];

	std::map<int, TH1D*> h_angleX; ///< Histogram of dx/dz from the eutracks tree. Only the first track in multi track event is handled and a cut is made on the nmatch criterium.
	std::map<int, TH1D*> h_angleY; ///< Histogram of dy/dz from the eutracks tree. Only the first track in multi track event is handled and a cut is made on the nmatch criterium.
	std::map<int, TH1D*> h_clusizeX; ///< Cluster size distribution of all clusters in x (columns) which passed the following cuts: central region and matched
	std::map<int, TH1D*> h_clusizeY; ///< Cluster size distribution of all clusters in y (rows) which passed the following cuts: central region and matched
	std::map<int, TH1D*> h_clusize; ///< Total cluster size distribution of all clusters which passed the following cuts: central region and matched
	std::map<int, TH2D*> h_clusizeXY; ///< Cluster size distribution of all clusters in x-y which passed the following cuts: central region and matched
	std::map<int, TH1D*> h_ecorrsX; ///< Distribution of eta correction in x
	std::map<int, TH1D*> h_ecorrsY; ///< Distribution of eta correction in y

	std::map<int, std::map<int, std::vector<TH1D*> > > hhh_res; ///< XY residual plots calculated with four different cluster position algorithm and a virtually unlimited number of cluster sizes
	std::map<int, std::map<int, std::vector<TH1D*> > > hhh_resX; ///< X residual plots calculated with four different cluster position algorithm and a virtually unlimited number of cluster sizes
	std::map<int, std::map<int, std::vector<TH1D*> > > hhh_resY; ///< Y residual plots calculated with four different cluster position algorithm and a virtually unlimited number of cluster sizes

	std::map<int, bool> use_minimum_sum_of_tot_per_cluster; ///< Says whether a cut on the minimum ToT sum of a cluster shall be applied.
	std::map<int, int> minimum_sum_of_tot_per_cluster; ///< If a cluster does not contain at least this number of entries, it is not used for the residual calculation.
	std::map<int, bool> use_minimum_entries_to_plot_histo; ///< Says whether a cut on the minimum number of entries in a plot to be plotted shall be applied.
	std::map<int, int> minimum_entries_to_plot_histo; ///< If a residual plot does not contain at least this number of entries, it is not drawn to a file and included in the root file during the finalize method.

	void getResiduals(const TBEvent* event, const TBTrack* track, std::vector<TH1D*>* histosX, std::vector<TH1D*>* histosY, std::vector<TH1D*>* histosAll);
	void fitAllClusResiduals(const TBCore* core, const char* histoTitle, const char* histoName, TH1 *h_residuals, double halfPitch, double xmin, double xmax);
	void fitResiduals(const TBCore* core, const char* histoTitle, const char* histoName, TH1* h_residuals, double halfPitch, double xmin, double xmax);
	void compareResidualsForOneClustersize(const TBCore* core, int iden, const char* histoTitle, const char* histoName, std::vector<TH1D*>* hh);
	
public:
	Residuals()
	{
		TBAnalysis::name = "Residuals";
	}
	
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);
	
};

double multiG(double* v, double* par);
double fitFunc(double* v, double* par);
double fitFuncBox(double* v, double* par);

// class factories
extern "C" TBAnalysis* create()
{
	return new Residuals;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* RESIDUALS_H */

