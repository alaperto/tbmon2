/* 
 * File:   EfficiencyVsRun.h
 * Author: daniel
 *
 * Created on 6. März 2014, 23:49
 */

#ifndef EFFICIENCYVSRUN_H
#define	EFFICIENCYVSRUN_H

#include "TBAnalysis.h"

class EfficiencyVsRun: public TBAnalysis
{
private:
	std::map<IDEN, TH1D*> histo_EffAllVsRun;
	std::map<IDEN, TH1D*> histo_EffMatchedVsRun;

	// n(tracks) vs run
	std::map<IDEN, std::map<int, int> > ntracks;
	// n(all hits) vs run
	std::map<IDEN, std::map<int, int> > nhits;
	// n(matched hits) vs run
	std::map<IDEN, std::map<int, int> > nmatched;

	// efficiency (all hits) vs run
	std::map<IDEN, TGraphErrors*> h_EffAllVsRun;
	// efficiency (matched hits) vs run
	std::map<IDEN, TGraphErrors*> h_EffMatchedVsRun;

	std::map<IDEN, double> effPlotMinY;

public:
	EfficiencyVsRun()
	{
		TBAnalysis::name = "EfficiencyVsRun";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);
};

// class factories
extern "C" TBAnalysis* create()
{
	return new EfficiencyVsRun;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* EFFICIENCYVSRUN_H */

