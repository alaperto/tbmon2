/* 
 * File:   TBCore.cc
 * 
 * Created on 5. Mai 2013
 */

#include <string>

#include "TBCore.h"

TBCore::TBCore(TBConfig* tbconfig)
{	TBCore::name = "TBCore";
	TBCore::core = this;
	TBCore::tbconfig = tbconfig;
	//TBCore::tboutput = new TBOutput(this);
	TBCore::triggerCount = 0;
	TBCore::tInFile = NULL;
	TBCore::treeVersionName = "version";
	TBCore::reco_version = NULL;
	TBCore::firstRun = TBCore::tbconfig->rawDataRuns.front();
	TBCore::lastRun = TBCore::tbconfig->rawDataRuns.back();
	
	//Graphic settings
	if(tbconfig->useAtlasStyle)
	{
		TBLOG(kINFO, "Applying ATLAS style settings...")
		SetAtlasStyle();
	}
	else
	{
		gROOT->SetStyle("Plain");
	}
	
	//gROOT->ProcessLine("gErrorIgnoreLevel = kError;");
	gROOT->ProcessLine("gErrorIgnoreLevel = 2000000;");
	gStyle->SetPalette(1);

	//TODO
	// check DUTs
	//TBCore::checkDUT()
	// create all DUTs
	TBCore::createDUT();
	// add all used eventbuilder
	TBCore::createPreprocess();
	// add all used analysis
	TBCore::createAnalysis();
	
	TBCore::output = new TBOutput2(this);
}

TBCore::~TBCore()
{
	TBCore::tTree = NULL;
	TBCore::tTreePreprocessing = NULL;
	//TBCore::tOutFile->Close();
	//TBCore::tFilePreprocessing->Close();
	TBCore::tInFile->Close();
	delete TBCore::tOutFile;
	if(TBCore::tbconfig->savePreprocessingFile)
	{
		delete TBCore::tFilePreprocessing;
	}
	delete TBCore::tInFile;
	//delete TBCore::tboutput;
	delete TBCore::output;
	/*
	for(auto& dut: TBCore::usedDUT)
	{
		delete dut;
		//dut = NULL;
	}
	TBCore::usedDUT.clear();
	for(auto& dut: TBCore::usedDUTMap)
	{
		delete dut.second;
		//dut = NULL;
	}
	TBCore::usedDUTMap.clear();
	 */
}

std::string TBCore::getParam(int iden, std::string name, std::string paramName) const
{
	if(tbconfig->param.find(iden) != tbconfig->param.end() && tbconfig->param[iden].find(name) != tbconfig->param[iden].end() && tbconfig->param[iden][name].find(paramName) != tbconfig->param[iden][name].end())
   {
		return tbconfig->param[iden][name][paramName];
	}
	else
	{
		return "";
	}
}

// TODO: addMask
void TBCore::createDUT()
{
	for(auto dutConfig: TBCore::tbconfig->dutConfig)
	{
		DUT* dut = new DUT(dutConfig);
		
		TBLOG(kINFO, "Adding DUT with iden " << dut->iden)
		TBCore::usedDUT.push_back(dut);
		TBCore::usedDUTMap[dut->iden] = dut;
	}
}

void TBCore::classOpen(std::string path, std::string className, std::string ext)
{
	void* classOpen = dlopen((path+className+ext).c_str(), RTLD_LAZY | RTLD_LOCAL);
	if(!classOpen)
	{
		TBLOG(kERROR, "Cannot load library: " << dlerror())
		exit(-1);
	}
	
	TBCore::openClasses[className] = classOpen;
	// reset errors
	dlerror();
}

void TBCore::classClose(void* classPrt)
{
	 dlclose(classPrt);
}

TBPreprocess* TBCore::loadPreprocess(std::string className)
{
	TBCore::classOpen(TBCore::tbconfig->sharedPreprocessPath, className, ".so");

	void* classPtr = TBCore::openClasses[className];
	// load symbols
	createTBP_t* create_tbp = (createTBP_t*)dlsym(classPtr, "create");
	char* dlsym_error = dlerror();
	if(dlsym_error)
	{
		TBLOG(kERROR, "Cannot load symbol " << className << ": " << dlsym_error)
		exit(-1);
	}
			
	// create an instance of the class
	TBPreprocess* tbp = create_tbp();
	
	return tbp;
}

void TBCore::unloadPreprocess(TBPreprocess* tbp)
{
	void* classPrt = TBCore::openClasses[tbp->name];
	destroyTBP_t* destroy_tbp = (destroyTBP_t*) dlsym(classPrt, "destroy");
	char* dlsym_error;
   dlsym_error = dlerror();
   if (dlsym_error)
	{
		TBLOG(kERROR, "Cannot load symbol destroy in " << tbp->name << ": " << dlsym_error)
      exit(-1);
   }
	 
	destroy_tbp(tbp);
	TBCore::classClose(classPrt);
}

TBAnalysis* TBCore::loadAnalysis(std::string className)
{
	TBCore::classOpen(TBCore::tbconfig->sharedAnalysisPath, className, ".so");

	void* classPtr = TBCore::openClasses[className];
	// load symbols
	createTBA_t* create_tba = (createTBA_t*)dlsym(classPtr, "create");
	char* dlsym_error = dlerror();
	if(dlsym_error)
	{
		TBLOG(kERROR, "Cannot load symbol " << className << ": " << dlsym_error)
		exit(-1);
	}
			
	// create an instance of the class
	TBAnalysis* tba = create_tba();
	
	return tba;
}

void TBCore::unloadAnalysis(TBAnalysis* tba)
{
	void* classPrt = TBCore::openClasses[tba->name];
	destroyTBA_t* destroy_tba = (destroyTBA_t*) dlsym(classPrt, "destroy");
	char* dlsym_error;
	dlsym_error = dlerror();
	if (dlsym_error)
	{
		TBLOG(kERROR, "Cannot load symbol destroy in " << tba->name << ": " << dlsym_error)
		exit(-1);
	}
	destroy_tba(tba);
	TBCore::classClose(classPrt);
}

void TBCore::createPreprocess()
{	
	// group -1
	TBCore::groupPreprocess[-1].push_back(TBCore::loadPreprocess("PixelMasker"));
	
	// group 0
	TBCore::groupPreprocess[0].push_back(TBCore::loadPreprocess("EuBuildTrack"));
	TBCore::groupPreprocess[0].push_back(TBCore::loadPreprocess("HotPixelFinder"));
	
	// group 1
	TBCore::groupPreprocess[1].push_back(TBCore::groupPreprocess[0][0]);
	TBCore::groupPreprocess[1].push_back(TBCore::loadPreprocess("ClusterFinder"));
	TBCore::groupPreprocess[1].push_back(TBCore::loadPreprocess("ClusterMatcher"));
	
	if(TBCore::tbconfig->useCheckAlign || TBCore::tbconfig->useGetEtaCorr)
	{
		TBCore::groupPreprocess[1].push_back(TBCore::loadPreprocess("CheckAlign"));
	}
	else
	{
		//TBCore::groupPreprocess[1].push_back(TBCore::loadPreprocess("chi2builder"));
	}
	
	/*if(TBCore::tbconfig->useGetEtaCorr)
	{
		// group 2
		TBCore::groupPreprocess[2].push_back(TBCore::groupPreprocess[0][0]);
		TBCore::groupPreprocess[2].push_back(TBCore::groupPreprocess[1][1]);
		TBCore::groupPreprocess[2].push_back(TBCore::groupPreprocess[1][2]);
		TBCore::groupPreprocess[2].push_back(TBCore::groupPreprocess[1][3]);
		TBCore::groupPreprocess[2].push_back(TBCore::groupPreprocess[1][4]);
		TBCore::groupPreprocess[2].push_back(TBCore::loadPreprocess("getetacorr"));
		
		// group 3
		TBCore::groupPreprocess[3].push_back(TBCore::groupPreprocess[0][0]);
		TBCore::groupPreprocess[3].push_back(TBCore::groupPreprocess[1][1]);
		TBCore::groupPreprocess[3].push_back(TBCore::groupPreprocess[1][2]);
		TBCore::groupPreprocess[3].push_back(TBCore::groupPreprocess[1][3]);
		TBCore::groupPreprocess[3].push_back(TBCore::groupPreprocess[1][4]);
		TBCore::groupPreprocess[3].push_back(TBCore::groupPreprocess[1][5]);
	}*/
	
	if(TBCore::tbconfig->useCheckAlign || TBCore::tbconfig->useGetEtaCorr)
	{
		// group 4
		TBCore::groupPreprocess[4].push_back(TBCore::groupPreprocess[0][0]);
		TBCore::groupPreprocess[4].push_back(TBCore::groupPreprocess[1][1]);
		TBCore::groupPreprocess[4].push_back(TBCore::groupPreprocess[1][2]);
		//TBCore::groupPreprocess[4].push_back(TBCore::groupPreprocess[1][3]);
		//TBCore::groupPreprocess[4].push_back(TBCore::groupPreprocess[1][3]);
		//TBCore::groupPreprocess[4].push_back(TBCore::loadPreprocess("chi2builder"));
	}
}

void TBCore::destroyPreprocess()
{
	TBCore::unloadPreprocess(TBCore::groupPreprocess[-1][0]);
	TBCore::unloadPreprocess(TBCore::groupPreprocess[0][0]);
	TBCore::unloadPreprocess(TBCore::groupPreprocess[0][1]);
	TBCore::unloadPreprocess(TBCore::groupPreprocess[1][1]);
	TBCore::unloadPreprocess(TBCore::groupPreprocess[1][2]);
	
	if(TBCore::tbconfig->useCheckAlign || TBCore::tbconfig->useGetEtaCorr)
	{
		TBCore::unloadPreprocess(TBCore::groupPreprocess[1][3]);
	}	
	/*TBCore::unloadPreprocess(TBCore::groupPreprocess[1][4]);
	
	if(TBCore::tbconfig->useCheckAlign)
	{
		TBCore::unloadPreprocess(TBCore::groupPreprocess[1][5]);
		TBCore::unloadPreprocess(TBCore::groupPreprocess[4][5]);
	}
	else
	{
		TBCore::unloadPreprocess(TBCore::groupPreprocess[1][5]);
	}
	if(TBCore::tbconfig->useGetEtaCorr)
	{
		TBCore::unloadPreprocess(TBCore::groupPreprocess[2][5]);
	}
	 */
}

void TBCore::createAnalysis()
{
	for(auto analysis: tbconfig->useAnalysis)
	{
		TBAnalysis* tba = TBCore::loadAnalysis(analysis);
	
		TBCore::runAnalysis.push_back(tba);
	}
}

void TBCore::destroyAnalysis()
{
	for(auto analysis: TBCore::runAnalysis)
	{
		TBCore::unloadAnalysis(analysis);
	}
}

void TBCore::loop()
{
	// redirect output to a logfile
	if(TBCore::tbconfig->logToFile)
	{
		std::string logFileName = (TBCore::tbconfig->outputPath)+(TBCore::tbconfig->rawDataName)+(std::to_string(firstRun)+"-"+std::to_string(lastRun))+".log";
		TBLOG(kINFO, "Output is being redirected to " << logFileName)
		std::freopen(logFileName.c_str(), "w", stdout);
	}
	
	// abort conditions 
	if(TBCore::tbconfig->rawDataRuns.size() < 1)
	{
		TBLOG(kERROR, "No runnumbers in rawDateRuns in mainConfig, nothing to do.")
		exit(0);
	}
	
	// allocate space for events
	for(list<DUT*>::iterator it = TBCore::usedDUT.begin(); it != TBCore::usedDUT.end(); it++)
	{
		TBCore::events[(*it)->iden] = new TBEvent((*it));
	}
	
	int runloopCount = 1;
	std::map<int, std::vector<TBPreprocess*> >::iterator last = --TBCore::groupPreprocess.end();
	// initialize eventbuilder
	for(std::map<int, std::vector<TBPreprocess*> >::iterator itt = TBCore::groupPreprocess.begin(); itt != TBCore::groupPreprocess.end(); itt++)
	{
		TBCore::triggerCount = 0;
		TBCore::preprocess = itt->second;
		
		// loop over all runs
		std::cout << "[ TBCore ]: " << runloopCount << ". loop over all runs. START" << std::endl;
		
		for(vector<TBPreprocess*>::iterator it = TBCore::preprocess.begin(); it != TBCore::preprocess.end(); it++)
		{
				if (TBCore::tbconfig->logLevel >= kINFO)
				{
					std::cout << "[ TBCore ]: Initialized Preprocess: \"" << (*it)->name << "\"" << std::endl;
				}
				(*it)->init(this);
		}
		
		if(itt == last)
		{
			for(vector<TBAnalysis*>::iterator it = TBCore::runAnalysis.begin(); it != TBCore::runAnalysis.end(); it++)
			{
				if (TBCore::tbconfig->logLevel >= kINFO)
				{
					std::cout << "[ TBCore ]: Initialized Analysis: \"" << (*it)->name << "\"" << std::endl;
				}
				(*it)->init(this);
			}
		}
		
		// add etacorr
		for(std::list<DUT*>::iterator it = TBCore::usedDUT.begin(); it != TBCore::usedDUT.end(); it++)
		{
			std::string fileNameEtaCorr = (TBCore::tbconfig->outputPath)+(TBCore::tbconfig->rawDataName)+"-GetEtaCorr-etacalib_dut"+std::to_string((*it)->iden)+".txt";
			struct stat statStruct;
			
			if( stat(fileNameEtaCorr.c_str(), &statStruct) == 0)
			{
				(*it)->ecorrs.addEcorr(fileNameEtaCorr.c_str());
			}
		}
		
		for(std::vector<int>::const_iterator currentRun = TBCore::tbconfig->rawDataRuns.begin(); currentRun != TBCore::tbconfig->rawDataRuns.end(); currentRun++)
		{
			TBCore::currentRun = *currentRun;

			// TODO: does file exist in data path? 
			char* fileName = new char[800];
			struct stat statStruct;
				sprintf(fileName, "%stbtrack%i.root", TBCore::tbconfig->rawDataPath.c_str(),TBCore::currentRun);
    				if( stat(fileName, &statStruct) != 0)
   				{
        				sprintf(fileName, "%stbtrack0%i.root", TBCore::tbconfig->rawDataPath.c_str(),TBCore::currentRun);
        				if( stat(fileName, &statStruct) != 0)
        				{
            					sprintf(fileName, "%stbtrack00%i.root", TBCore::tbconfig->rawDataPath.c_str(),TBCore::currentRun);
            					if( stat(fileName, &statStruct) != 0)
            					{
                					sprintf(fileName, "%stbtrack000%i.root", TBCore::tbconfig->rawDataPath.c_str(),TBCore::currentRun);
                					if( stat(fileName, &statStruct) != 0)
                					{
                    						sprintf(fileName, "%stbtrack0000%i.root", TBCore::tbconfig->rawDataPath.c_str(),TBCore::currentRun);
                    						if (stat(fileName, &statStruct) != 0)
                    						{
                        						cout << "[ TBCore ]: loop(): Can't find file for run" << TBCore::currentRun << " (tried 1 through 4 prepending 0s) - SKIPPING" <<endl;
                        						continue;
                    						}
                					}
            					}
        				}
    				}

				// open trackfile with current runnumber
				TBCore::tInFile = new TFile(fileName, "READ"); 

				// load tree
				TBCore::tTree = dynamic_cast<TTree*>(TBCore::tInFile->Get(TBCore::treeVersionName.c_str()));

				// test if tree is actually loaded
				if (TBCore::tTree == NULL)
				{
					cout<<"[ TBCore ]: Old version of tbtrack file detected"<<endl;

					// set version number for old tbtrack file
					TBCore::tbtrackVersion = 0.0;

					TBCore::treeName = "eutracks";
					TBCore::tTree = dynamic_cast<TTree*>(TBCore::tInFile->Get(TBCore::treeName.c_str()));
				}
				else
				{

					// get branch and first entry
					TBCore::tTree->SetBranchAddress("no", &(TBCore::reco_version));
					TBCore::tTree->GetEntry(0);
					TBCore::tbtrackVersion = TBCore::reco_version->at(0);

					cout<<"[ TBCore ]: Version " << TBCore::tbtrackVersion << " of tbtrack file detected"<<endl;

					TBCore::treeName = "tracks";
					TBCore::tTree = dynamic_cast<TTree*>(TBCore::tInFile->Get(TBCore::treeName.c_str()));

					// always use z position from reco
					TBCore::tbconfig->useRecoZ = true;
				}

				//TBCore::tFile->cd();
				if(TBCore::tbconfig->logLevel >= kINFO )
				{
					std::cout << "[ TBCore ]: Processing: " << fileName << std::endl;
				}
			
				delete fileName;

				// TODO
				// create preprocessing file for the run
				if(TBCore::tbconfig->savePreprocessingFile)
				{
					// TODO abfrage, ob daten ueberschrieben werden oder eine neue datei erzeugt wird.
					std::string preprocessingFileName = (TBCore::tbconfig->outputPath)+(TBCore::tbconfig->preprocessingDataPath)+(TBCore::tbconfig->rawDataName)+(std::to_string(TBCore::currentRun))+"_"+(TBCore::tbconfig->addingPreprocessingFile)+"."+(TBCore::tbconfig->rawDataNameExtension);
					TBCore::tFilePreprocessing = new TFile(preprocessingFileName.c_str(), "RECREATE");
					TBCore::tFilePreprocessing->cd();
					TBCore::tTreePreprocessing = new TTree(TBCore::tbconfig->addingPreprocessingFile.c_str(), TBCore::tbconfig->addingPreprocessingFile.c_str());
				}

				// initialize run in DUT and eventbuilder
				// loop over all DUT
				for(list<DUT*>::iterator it = TBCore::usedDUT.begin(); it != TBCore::usedDUT.end(); it++ )
				{
					//core.tFile->cd();
					// initialize run in eventbuilder
					(*it)->initRun(TBCore::currentRun);

					// prepare preprocessing file
					if(TBCore::tbconfig->savePreprocessingFile)
					{
						//TBCore::tFilePreprocessing->mkdir((to_string((*it)->iden)).c_str());
					}
				}

				for(vector<TBPreprocess*>::iterator it = TBCore::preprocess.begin(); it != TBCore::preprocess.end(); it++)
				{
					if((*it)->name.compare("EuBuildTrack") == 0)
					{
						/*for(std::list<DUT*>::iterator iit = TBCore::usedDUT.begin(); iit != TBCore::usedDUT.end(); iit++)
						{
							(*it)->addTranslation((*iit)->iden, TBCore::currentRun, TBCore::currentTranslationValueX[(*iit)->iden][TBCore::currentRun], TBCore::currentTranslationValueY[(*iit)->iden][TBCore::currentRun]);
							TBCore::currentTranslationValueX.clear();
							TBCore::currentTranslationValueY.clear();
						}*/
					}
					(*it)->initRun((this));

					if (TBCore::tbconfig->logLevel >= kINFO)
					{
						std::cout << "[ TBCore ]: Initialize run " << TBCore::currentRun << "  Preprocess: \"" << (*it)->name << "\"" << std::endl;
					}
					//core.tFile->cd();
				}
				
				if(itt == last)
				{
					for(vector<TBAnalysis*>::iterator iit = TBCore::runAnalysis.begin(); iit != TBCore::runAnalysis.end(); iit++)
					{
						(*iit)->initRun(this);
						if (TBCore::tbconfig->logLevel >= kINFO)
						{
							std::cout << "[ TBCore ]: Initialize run " << TBCore::currentRun << "  Analysis: \"" << (*iit)->name << "\"" << std::endl;
						}
					}
				}

				int nentries = TBCore::tTree->GetEntries();
				int run_until_event = nentries;

				if(TBCore::tbconfig->maxNumOfTrigToBeProcPerRun != -1)
				{
					if(TBCore::tbconfig->maxNumOfTrigToBeProcPerRun <= nentries)
					{
						run_until_event = TBCore::tbconfig->maxNumOfTrigToBeProcPerRun;
					} else {
						std::cout << "[ TBCore ]: MaxNumOfTrigToBeProcPerRun is set to " << TBCore::tbconfig->maxNumOfTrigToBeProcPerRun << " but there are only " << nentries << " triggers available in run " << TBCore::currentRun << ". This setting will be subsequently ignored." << std::endl;
					}
				}

				for(TBCore::currentEntry = 1; TBCore::currentEntry<run_until_event; TBCore::currentEntry++)
				{
					TBCore::triggerCount++;
					//cout << "Eventnummer: " << currentEntry << endl;
					// Run event builders
					// Clear instead of recreate too speed things up a bit
					for(list<DUT*>::iterator it = TBCore::usedDUT.begin(); it != TBCore::usedDUT.end(); it++)
					{
						//Clear instead of recreate too speed things up a bit
						TBCore::events[(*it)->iden]->clear();
					}

					for(vector<TBPreprocess*>::iterator iit = TBCore::preprocess.begin(); iit != TBCore::preprocess.end(); iit++)
					{
						(*iit)->initEvent(this);
					}

					for(std::map<int,TBEvent*>::iterator it = events.begin(); it != events.end(); it++)
					{
						for(vector<TBPreprocess*>::iterator iit = TBCore::preprocess.begin(); iit != TBCore::preprocess.end(); iit++)
						{
							
								(*iit)->buildEvent(this, (*it).second);
						}
						
						if(itt == last)
						{
							printEvent((*it).first);
							/*if(TBCore::tbconfig->savePreprocessingFile && (*it).second.fBase == event::kGood)
							{
								TBCore::tboutput->saveEventToPreprocessingFile((*it).second, (*it).first);
							}*/
							
							for(vector<TBAnalysis*>::iterator iit = TBCore::runAnalysis.begin(); iit != TBCore::runAnalysis.end(); iit++)
							{	
									(*iit)->event(this, (*it).second);
							}
						}
					}
				}

				for(vector<TBPreprocess*>::iterator it = TBCore::preprocess.begin(); it != TBCore::preprocess.end(); it++)
				{
					(*it)->finalizeRun((this));
					if (TBCore::tbconfig->logLevel >= kINFO)
					{
						std::cout << "[ TBCore ]: Finalize run " << TBCore::currentRun << "  Preprocess: \"" << (*it)->name << "\"" << std::endl;
					}

				}
				
				if(itt == last)
				{
					for(vector<TBAnalysis*>::iterator iit = TBCore::runAnalysis.begin(); iit != TBCore::runAnalysis.end(); iit++)
					{
						(*iit)->finalizeRun(this);
						if (TBCore::tbconfig->logLevel >= kINFO)
						{
							std::cout << "[ TBCore ]: Finalize run " << TBCore::currentRun << "  Analysis: \"" << (*iit)->name << "\"" << std::endl;
						}
					}
				}

				// close preprocessing root file
				if(TBCore::tbconfig->savePreprocessingFile)
				{
					TBCore::tFilePreprocessing->WriteTObject(TBCore::tTreePreprocessing);
					TBCore::tFilePreprocessing->Close();
				}

					if(TBCore::tbconfig->maxNumOfTrigToBeProcPerRun != -1)
					{
						std::cout << "[ TBCore ]: Maximum number of triggers to be processed in one run reached (first one is always ignored): " << TBCore::currentEntry << std::endl;
					}
				TBCore::tInFile->Close();
				}

				// finalize eventbuilder
				for (vector<TBPreprocess*>::iterator it = TBCore::preprocess.begin(); it != TBCore::preprocess.end(); it++)
				{
					(*it)->finalize(this);
					if (TBCore::tbconfig->logLevel >= kINFO)
					{
						std::cout << "[ TBCore ]: Finalize Preprocess: \"" << (*it)->name << "\"" << std::endl;
					}
				}
		
				// reset comments on output
				this->output->currentPreprocessCuts = "";

				if(itt == last)
				{
					for(vector<TBAnalysis*>::iterator it = TBCore::runAnalysis.begin(); it != TBCore::runAnalysis.end(); it++)
					{
						(*it)->finalize(this);
						if (TBCore::tbconfig->logLevel >= kINFO)
						{
							std::cout << "[ TBCore ]: Finalize Analysis: \"" << (*it)->name << "\"" << std::endl;
						}
					}
				}
		
		// loop over all runs
		std::cout << "[ TBCore ]: " << runloopCount << ". loop over all runs. END" << std::endl;
		std::cout << "[ TBCore ]: ================== " << std::endl;
		runloopCount++;
	}
	
	TBLOG(kINFO, "Total " << TBCore::triggerCount  << " triggers");
	
	TBCore::destroyPreprocess();
	TBCore::destroyAnalysis();
	
	// write configs into output
	std::string path;
	path = TBCore::tbconfig->outputPath + TBCore::tbconfig->configPath + "mainConfig.cfg";
	output->writeFilesIntoOutputFile(path);
	path = TBCore::tbconfig->outputPath + TBCore::tbconfig->configPath + "analysisConfig.cfg";
	output->writeFilesIntoOutputFile(path);
	for(auto p: TBCore::tbconfig->useDUT)
	{
		path = TBCore::tbconfig->generalDutConfigPath + (p.second);
		output->writeFilesIntoOutputFile(path);
	}
	if(TBCore::tbconfig->logToFile)
	{
		path = (TBCore::tbconfig->outputPath)+(TBCore::tbconfig->rawDataName)+(std::to_string(firstRun)+"-"+std::to_string(lastRun))+".log";
		output->writeFilesIntoOutputFile(path);
	}
}

void TBCore::printDut(DUT* dut)
{
	for(int i=0; i<dut->getNrows(); i++)
	{
		for(int j=0; j<dut->getNcols(); j++)
		{
			//std::cout << ":(" << j << "," << i << ")e-" << dut->pixelArray[j][i].getGeometry()->isEdgePixel() << ":" << std::ends;
			std::cout << dut->pixelArray[j][i].getGeometry()->isEdgePixel() << ":" << std::ends;
		}
		std::cout << std::endl;
	}
}

void TBCore::printEvent(int iden)
{
	if(false)
	{
		TBLOG(kINFO, "Event trigger " << TBCore::triggerCount << " DUT "  << iden <<"\n\t\t"
			  << "raw Hits count " << events[iden]->rawHits.size() << "\n\t\t"
			  << "Hits count " << events[iden]->hits.size() << "\n\t\t"
			  << "raw Tracks " << events[iden]->rawTracks.size() << "\n\t\t"
			  << "Tracks " << events[iden]->tracks.size() << "\n\t\t"
			  << "raw Clusters " << events[iden]->rawClusters.size() << "\n\t\t"
			  << "Clusters " << events[iden]->clusters.size());
	}
}
