/* 
 * File:   TBEvent.cc
 * Author: Daniel Kalin
 * 
 * Created on 24. Januar 2014
 * 
 * v1.0
 */

#include "TBEvent.h"

// TBHit
TBHit::TBHit()
{
	TBHit::fMaskedHit = kUnknown;
	TBHit::fEdgeHit = kUnknown;
	TBHit::fCentralHit = kUnknown;
	TBHit::fLv1 = kUnknown;
}

TBHit::~TBHit()
{}

int TBHit::getLv1()
{
	return lv1;
}

// TBTrack
TBTrack::TBTrack()
{
	TBTrack::fMatchedHit = kUnknown;
	TBTrack::fMatchedCluster = kUnknown;
	TBTrack::fMatchedTrack = kUnknown;
	TBTrack::fTrackChi2 = kUnknown;
	TBTrack::fTrackEdgeRegion = kUnknown;
	TBTrack::fTrackCentralRegion = kUnknown;
	TBTrack::fTrackMaskedRegion = kUnknown;
	TBTrack::fEtaCorrections = kUnknown;
	TBTrack::fEtaCut = kUnknown;
	
	TBTrack::matchedHit = NULL;
	TBTrack::matchedHitType = -1;
	TBTrack::matchedCluster = NULL;
}

TBTrack::~TBTrack()
{
	TBTrack::matchedHit = NULL;
	TBTrack::matchedCluster = NULL;
}

// TBEvent
TBEvent::TBEvent(DUT* dut)
{
	TBEvent::dut = dut;
	TBEvent::iden = dut->iden;
	TBEvent::fRawHits = kBad;
	TBEvent::fHits = kUnknown;
	TBEvent::fClusters = kUnknown;
	TBEvent::fRawTracks = kBad;
	TBEvent::fTracks = kUnknown;
}

TBEvent::~TBEvent()
{
	TBEvent::dut = NULL;
		
	for(auto ptr: TBEvent::rawHits)
	{
		delete ptr;
	}
	TBEvent::rawHits.clear();
	TBEvent::hits.clear();
		
	for(auto ptr: TBEvent::rawClusters)
	{
		delete ptr;
	}
	TBEvent::rawClusters.clear();
	TBEvent::clusters.clear();
		
	for(auto ptr: TBEvent::rawTracks)
	{
		delete ptr;
	}
	TBEvent::rawTracks.clear();
	TBEvent::tracks.clear();
}

void TBEvent::clear()
{
	for(auto ptr: TBEvent::rawHits)
	{
		delete ptr;
	}
	TBEvent::rawHits.clear();
	TBEvent::hits.clear();
		
	for(auto ptr: TBEvent::rawClusters)
	{
		delete ptr;
	}
	TBEvent::rawClusters.clear();
	TBEvent::clusters.clear();
		
	for(auto ptr: TBEvent::rawTracks)
	{
		delete ptr;
	}
	TBEvent::rawTracks.clear();
	TBEvent::tracks.clear();
		
	TBEvent::fRawHits = kBad;
	TBEvent::fHits = kUnknown;
	TBEvent::fRawClusters = kUnknown;
	TBEvent::fClusters = kUnknown;
	TBEvent::fRawTracks = kBad;
	TBEvent::fTracks = kUnknown;
}

void TBEvent::addRawHit(int trig, int iden, int col, int row, int tot, int lv1)
{
	TBHit* tbhit = new TBHit();
	tbhit->iden = iden;
	tbhit->col = col;
	tbhit->row = row;
	tbhit->tot = tot;
	tbhit->lv1 = lv1;
	tbhit->trig = trig;
	
	TBEvent::rawHits.push_back(tbhit);
	TBEvent::fRawHits = kGood;
}

void TBEvent::addRawTrack(int trig, int iden, int trackNum, double trackX, double trackY, double dxdz, double dydz, double chi2, double ndof)
{
	TBTrack* tbtrack = new TBTrack();
	tbtrack->trig = trig;
	tbtrack->iden = iden;
	tbtrack->trackNum = trackNum;
	tbtrack->trackX = trackX;
	tbtrack->trackY = trackY;
	tbtrack->trackCol = -1;
	tbtrack->trackRow = -1;
	tbtrack->dxdz = dxdz;
	tbtrack->dydz = dydz;
	tbtrack->chi2 = chi2;
	tbtrack->ndof = ndof;
	
	TBEvent::rawTracks.push_back(tbtrack);
	TBEvent::fRawTracks = kGood;
}

void TBEvent::addRawCluster()
{
	TBCluster* tbcluster = new TBCluster();
	
	TBEvent::rawClusters.push_back(tbcluster);
}
