#include "TBConfig.h"

// default setting
std::string TBConfig::sharedEventBuilderPath = "eventbuilders/";
std::string TBConfig::sharedPreprocessPath = "preprocess/";
std::string TBConfig::sharedAnalysisPath = "analysis/";
std::string TBConfig::mainConfigName = "mainConfig.cfg";
std::string TBConfig::analysisConfigName = "analysisConfig.cfg";
std::string TBConfig::rawDataNameExtension = "root";
std::string TBConfig::preprocessingDataPath = "preprocessing/";
std::string TBConfig::plotPath = "plot/";
std::string TBConfig::configPath = "config/";
std::string TBConfig::extMaskPath = "ext_mask/";
std::string TBConfig::generalDutConfigPath = "config/DUT/";
std::string TBConfig::draftConfigPath = "config/draft/";
