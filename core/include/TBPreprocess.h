/* 
 * File:   TBPreprocess.h
 * Author: Daniel Kalin
 *
 * Created on 24. Januar 2014
 * 
 * v1.0
 */

#ifndef TBPREPROCESS_H
#define	TBPREPROCESS_H

#include <algorithm>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <stdlib.h>
#include <string>
#include <vector>

#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TTree.h>

#include "TBCore.h"
#include "TBEvent.h"
#include "TBDut.h"
//#include "tbutils.h"
//#include "clusters.h"

class TBPreprocess
{
public:
	typedef int IDEN;
	
	std::string name;

	TBPreprocess()
	{
		TBPreprocess::name = "Unknown";
	};
	virtual ~TBPreprocess() {;};
  
   virtual void init(TBCore* core) {};
	virtual void initRun(TBCore* core){;};
	virtual void initEvent(TBCore* core) {;};
	virtual void buildEvent(TBCore* core, TBEvent* event) {};
	virtual void finalizeRun(TBCore* core) {;};
	virtual void finalize(TBCore* core) {;};
	virtual void addTranslation(int iden, int run, double shiftX, double shiftY) {;};
	virtual void clear(){;};
};

typedef TBPreprocess* createTBP_t();
typedef void destroyTBP_t(TBPreprocess*);

#endif	/* TBPREPROCESS_H */

