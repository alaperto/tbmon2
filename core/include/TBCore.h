/* 
 * File:   TBCore.h
 *
 * Created on 5. Mai 2013
 */

#ifndef TBCORE_H
#define	TBCORE_H

#define TBLOG(level, msg) if(level <= core->tbconfig->logLevel) cout << "[ " << this->name << " ]: " << msg << endl;
enum logLevel_t{ kERROR, kINFO, kDEBUG, kDEBUG2, kDEBUG3};

#include <list>
#include <vector>
#include <string>
#include <dlfcn.h>

class TBCore;
class TBOutput;
class TBOutput2;
class TBPreprocess;
#include "TBConfig.h"
#include "TBDut.h"
#include "TBEvent.h"
#include "AtlasStyle.h"


#include <TFile.h>
#include <TDirectoryFile.h>
#include <TTree.h>

#include "TBPreprocess.h"
#include "TBAnalysis.h"


class TBTranslation
{
public:
	double shiftX;
	double shiftY;
	
	TBTranslation(double shiftX, double shiftY)
	{
		TBTranslation::shiftX = shiftX;
		TBTranslation::shiftY = shiftY;
	};
	~TBTranslation();
};

class TBCore
{
public:
	std::string name;
	TBCore* core;
	// programm run counter
	int triggerCount; // trigger ID
	int currentRun;
	int firstRun;
	int lastRun;
	int currentEntry;
	
	// main Config
	TBConfig* tbconfig;
	// class for output function
	TBOutput* tboutput;
	TBOutput2* output;
	// ROOT TFile
	TFile* tOutFile;
	TFile* tInFile;
	TTree* tTree;
	std::string treeName;
	std::string treeVersionName;
	double tbtrackVersion;
	std::vector<double>* reco_version;
	// DUT
	std::list<DUT*> usedDUT; 
	std::map<int, DUT*> usedDUTMap;
	// events
	std::map<int, TBEvent*> events;
	
	std::vector<TBAnalysis*> runAnalysis;
	std::vector<TBPreprocess*> preprocess;
	std::map<int, std::vector<TBPreprocess*> > groupPreprocess;
	std::map<std::string, void*> openClasses;
	
	// preprocessing
	TFile* tFilePreprocessing;
	TTree* tTreePreprocessing;
	
	// current Translation values for the current run
	std::map<int, std::map<int, TBTranslation*> >translations;
	
public:
	TBCore(TBConfig* tbconfig);
	~TBCore();
	void testDUTConfig();
	std::string getParam(int iden, std::string name, std::string paramName) const;
	//const std::string getParam(int iden, std::string name, std::string paramName){ return getParam(iden, name, paramName); };
	void loop();
	void printDut(DUT* dut);
	
private:
	void createDUT();
	void classOpen(std::string path, std::string className, std::string ext);
	void classClose(void* classPrt);
	TBPreprocess* loadPreprocess(std::string className);
	void unloadPreprocess(TBPreprocess* className);
	TBAnalysis* loadAnalysis(std::string className);
	void unloadAnalysis(TBAnalysis* tba);
	void createPreprocess();
	void createAnalysis();
	void destroyPreprocess();
	void destroyAnalysis();
	void printEvent(int iden);
};

#include "TBOutput.h"
#include "TBOutput2.h"


#endif	/* TBCORE_H */

