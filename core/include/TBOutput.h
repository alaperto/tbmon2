/* 
 * File:   TBOutput.h
 *
 * Created on 5. Mai 2013
 */

#ifndef TBOUTPUT_H
#define	TBOUTPUT_H

#include <assert.h>
#include <stdio.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <vector>

#include <sys/stat.h>

#include <TFile.h>
#include <TCanvas.h>
#include <TNamed.h>
#include <TH1.h>
#include <TH1D.h>

class TBCore;
class TBOutput;
class TBOutput2;
#include "TBCore.h"

class TBOutput
{
private:
	TBCore* core;
	//StoreEvent* storeevent;
public:
	//void saveEventToPreprocessingFile(const Event& event, const int iden);
	TBOutput(TBCore* core);
	virtual ~TBOutput();
	char* buildHistName(const std::string analysisName, const char* histoName) const;
	void setTitle( const std::string analysisName, const char* histoName, TH1* histo) const;
	void saveToFile(const std::string analysisName, const char* histoName, TNamed* histo) const;
	void drawToFile(const std::string analysisName, const char* histoName, const char* drawOpts, TNamed* h1, TNamed* h2=NULL, TNamed* h3=NULL, TNamed* h4 = NULL) const;
	void drawToFile(const std::string analysisName, const char* histoName, TNamed* h1, TNamed* h2=NULL, TNamed* h3=NULL, TNamed* h4 = NULL ) const;
	void drawToFile(const std::string analysisName, const char* histoName, const char* drawOpts, const std::vector<TNamed*>* hh) const;
	void drawToFile(const std::string analysisName, const char* histoName, const std::vector<TNamed*>* hh) const;
	void dumpToLisp(const std::string analysisName, const char* histoName, TH1D* histo) const;
	void drawAndSave(const std::string analysisName, const char* histoName, TNamed* histo) const;
	void drawAndSave(const std::string analysisName, const char* histoName, TNamed* histo, int dutIndex) const;
	void drawAndSave(const std::string analysisName, const char* histoName, const char* drawOpts, TNamed* histo) const;
  char* getOutStreamName(const std::string analysisName, const char* name) const;
private:

};

#endif	/* TBOUTPUT_H */

